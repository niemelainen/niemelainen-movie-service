# README #

Back-end application for **Niemelainen Movie Service**, a software which a user can find movies by genres. 

## Install
Application uses Java 11, Gradle and uses Git as version control 

* [Java 11][1]
* [Gradle][2]
* [Git][1]

### How to run

You can run the application from your IDE or hitting terminal on project's root:
 > ./gradlew bootRun

Server listens on 

> localhost:8080

Example end point request: 

>http://localhost:8080/api/recommendation?movieGenre=Horror&streamingService=Netflix&minimumIMDBRating=6.7

### Made By ###

Antti Niemeläinen

Hit a follow: [twitch.tv/niemelainen][4] 👍


[1]: https://www.oracle.com/java/technologies/javase/jdk11-archive-downloads.html "Java 11"
[2]: https://git-scm.com/ "Git"
[3]: https://gradle.org/ "Gradle"
[4]: https://twitch.tv/niemelainen "Gradle"