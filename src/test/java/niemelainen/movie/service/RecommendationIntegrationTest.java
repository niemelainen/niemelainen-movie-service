package niemelainen.movie.service;

import niemelainen.movie.service.dto.Movie;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.annotation.PostConstruct;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Objects;

import static niemelainen.movie.service.enums.MovieGenre.Horror;
import static niemelainen.movie.service.enums.StreamingService.Netflix;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@SpringBootTest
class RecommendationIntegrationTest {

    private TestRestTemplate template;

    private URL base;

    @PostConstruct
    public void init() throws MalformedURLException {
        int port = 8080;
        this.base = new URL("http://localhost:" + port + "/api/recommendation");
        template = new TestRestTemplate();
    }

    @Test
    void getMoviesFromNetflixByGenre() {
        String request = base.toString() + "?movieGenre=" + Horror + "&streamingService=" + Netflix;
        ResponseEntity<Movie[]> response = template.getForEntity(request, Movie[].class);
        assertThat(response.getStatusCode(), is(HttpStatus.OK));
        assertThat(Objects.requireNonNull(response.getBody()).length, is(1));
    }

    @Test
    void getMoviesFromNetflixByGenreAndMinimumIMDRating() {
        String request = base.toString() + "?movieGenre=" + Horror + "&streamingService=" + Netflix + "&minimumIMDBRating=6.7";
        ResponseEntity<Movie[]> response = template.getForEntity(request, Movie[].class);
        assertThat(response.getStatusCode(), is(HttpStatus.OK));
        assertThat(Objects.requireNonNull(response.getBody()).length, is(1));
    }

    @Test
    void getMoviesFromNetflixByGenreAndMinimumIMDRatingOfTooHighNumberFails() {
        String request = base.toString() + "?movieGenre=" + Horror + "&streamingService=" + Netflix + "&minimumIMDBRating=18.8";
        ResponseEntity<String> response = template.exchange(request, HttpMethod.GET, null, String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.BAD_REQUEST));
    }

    @Test
    void getMoviesFromNetflixByGenreAndMinimumIMDRatingOfTooLowNumberFails() {
        String request = base.toString() + "?movieGenre=" + Horror + "&streamingService=" + Netflix + "&minimumIMDBRating=-1.8";
        ResponseEntity<String> response = template.exchange(request, HttpMethod.GET, null, String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.BAD_REQUEST));
    }

    @Test
    void getMoviesFromWithoutStreamingServiceFails() {
        String request = base.toString() + "?movieGenre=" + Horror;
        ResponseEntity<String> response = template.getForEntity(request, String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.BAD_REQUEST));
    }

    @Test
    void getMoviesFromNetflixWithUnknownGenreFails() {
        String request = base.toString() + "?movieGenre=NotAGenre";
        ResponseEntity<String> response = template.getForEntity(request, String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.BAD_REQUEST));
    }

    @Test
    void getMoviesFromNetflixWithoutGenreFails() {
        String request = base.toString() + "?streamingService=" + Netflix;
        ResponseEntity<String> response = template.getForEntity(request, String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.BAD_REQUEST));
    }
}
