package niemelainen.movie.service.service;

import niemelainen.movie.service.Logger;
import niemelainen.movie.service.dto.Movie;
import niemelainen.movie.service.enums.MovieGenre;
import niemelainen.movie.service.enums.StreamingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service("RecommendationService")
public class RecommendationService extends Logger {

    @Autowired
    private IMDBApiService imdbApiService;

    @Autowired
    private NetflixApiService netflixApiService;

    /**
     * Find movies by genre and minimum rating.
     * Note that Movie can have multiple genres.
     *
     * @param streamingService  User pick of the streaming service.
     * @param movieGenre        Genre of the movie.
     * @param minimumIMDBRating Minimum rating which movie can have on IMDB, not required.
     * @return List<Movie> List of movies which belongs to that genre.
     */
    public List<Movie> getMovies(StreamingService streamingService, MovieGenre movieGenre, Double minimumIMDBRating) {
        List<Movie> movies = new ArrayList<>();
        //TODO:
        // Fetching movies by minimum rating would be good feature and let's hope
        // IMDB has an API call for that, otherwise need to think how IMDB can be
        // used on this application
        if (minimumIMDBRating != null) {
            validateFetchParams(minimumIMDBRating);
            movies = imdbApiService.getMoviesByGenre(movieGenre, minimumIMDBRating);
        }
        return findMoviesFromStreamingService(streamingService, movieGenre, movies);
    }

    private List<Movie> findMoviesFromStreamingService(StreamingService streamingService, MovieGenre movieGenre, List<Movie> movies) {
        switch (streamingService) {
            case Netflix:
                return netflixApiService.findMovies(movieGenre, movies);
            // To be implemented
            // case HBO: return Collections.emptyList();
            // case Prime: return Collections.emptyList();
            default:
                return Collections.emptyList();
        }
    }

    private void validateFetchParams(Double minimumIMDBRating) {
        if (minimumIMDBRating < 0.0 || minimumIMDBRating > 10.0) {
            throw new InvalidParameterException("Minimum IMDB rating must be on range of 0.0 - 10.0");
        }
    }
}
