package niemelainen.movie.service.service;

import niemelainen.movie.service.JsonBodyHandler;
import niemelainen.movie.service.Logger;
import niemelainen.movie.service.dto.Actor;
import niemelainen.movie.service.dto.Movie;
import niemelainen.movie.service.enums.MovieGenre;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service("NetflixApiService")
public class NetflixApiService extends Logger {

    @Value("${imdb.api}")
    private String netflixApi;


    /**
     * Find movies by genre or with list of movie names.
     * Note that Movie can have multiple genres.
     *
     * @param movieGenre         Genre of the movie
     * @param IMDBMoviesByRating Movie titles gathered from IMDB based on genre and minimum rating.
     * @return List<Movie> List of movies found from Netflix
     */
    public List<Movie> findMovies(MovieGenre movieGenre, List<Movie> IMDBMoviesByRating) {
        if (!IMDBMoviesByRating.isEmpty()) {
            return getMoviesByName(IMDBMoviesByRating);
        }
        return getMoviesByGenre(movieGenre);
    }

    // If there are going to be more API requests, one method for calling API should be implemented
    // with parameter of URI and necessary params
    private List<Movie> getMoviesByGenre(MovieGenre genre) {
        // TODO: Add correct API call
        try {
            HttpClient client = HttpClient.newHttpClient();
            HttpRequest request = HttpRequest.newBuilder(
                            URI.create(netflixApi))
                    .header("accept", "application/json")
                    .build();
            //TODO:
            // response is now received as String until we know the Netflix's model
            //var response = client.send(request, new JsonBodyHandler<>(String.class));
            // go through found movies by mapping them to Movie entity and add them to list
            List<Movie> movies = new ArrayList<>();
            //NetflixModelToMovie(response.body().get());

        } catch (Exception e) {
            LOGGER.error("Failed to fetch movies from Netflix: {}", e.getMessage());
        }
        return Collections.singletonList(dummyData());
    }

    private List<Movie> getMoviesByName(List<Movie> IMDBMoviesByRating) {
        //TODO: Add correct API call
        // Can you find movies by list of names? If yes one call is enough
        // otherwise need to make multiple calls for fetching movies
        try {
            HttpClient client = HttpClient.newHttpClient();
            HttpRequest request = HttpRequest.newBuilder(
                            URI.create(netflixApi))
                    .header("accept", "application/json")
                    .build();
            //TODO:
            // response is now received as String until we know the Netflix's model
            //var response = client.send(request, new JsonBodyHandler<>(String.class));
            // go through found movies by mapping them to Movie entity and add them to list
            List<Movie> movies = new ArrayList<>();
            //NetflixModelToMovie(response.body().get());

        } catch (Exception e) {
            LOGGER.error("Failed to fetch movies from Netflix: {}", e.getMessage());
        }
        return Collections.singletonList(dummyData());
    }

    private Movie NetflixModelToMovie(String IMDBModel) {
        Movie movie = new Movie();
        //TODO: use Netflix's model to map data to Movie entity
        return movie;
    }

    private Movie dummyData() {
        Movie movie = new Movie();
        movie.setName("Movie");
        movie.setIMDBRating(7.7);
        movie.setDirector("Director");
        movie.setGenres(new ArrayList<>(Collections.singleton(MovieGenre.Horror)));
        movie.setActors(new ArrayList<>(Collections.singleton(new Actor())));
        return movie;
    }
}
