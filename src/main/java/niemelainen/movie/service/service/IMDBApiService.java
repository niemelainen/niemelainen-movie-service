package niemelainen.movie.service.service;

import niemelainen.movie.service.JsonBodyHandler;
import niemelainen.movie.service.Logger;
import niemelainen.movie.service.dto.Actor;
import niemelainen.movie.service.dto.Movie;
import niemelainen.movie.service.enums.MovieGenre;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

@Service("IMDBApiService")
public class IMDBApiService extends Logger {

    @Value("${imdb.api}")
    private String imdbApi;

    public List<Movie> getMoviesByGenre(MovieGenre genre, Double minimumIMDBRating) {
        //TODO: Add correct API call
        // Waiting to get correct API for fetching IMDB ratings and other data
        try {
            HttpClient client = HttpClient.newHttpClient();
            HttpRequest request = HttpRequest.newBuilder(
                            URI.create(imdbApi))
                    .header("accept", "application/json")
                    .build();
            //response is now received as String until we know the IMDB's model
            var response = client.send(request, new JsonBodyHandler<>(String.class));
            //TODO:
            // go through IMDB suggestions by mapping them to Movie entity and add them to list
            List<Movie> movies = new ArrayList<>();
            IMDBModelToMovie(response.body().get());

        } catch (Exception e) {
            LOGGER.error("Failed to fetch movies from IMDB: {}", e.getMessage());
        }
        return Collections.singletonList(dummyData());
    }

    private Movie IMDBModelToMovie(String IMDBModel) {
        Movie movie = new Movie();
        //TODO:
        // use IMDB's model to map data to Movie entity
        return movie;
    }

    private Movie dummyData() {
        Movie movie = new Movie();
        movie.setName("Movie");
        movie.setIMDBRating(ThreadLocalRandom.current().nextDouble(1.0, 10.0));
        movie.setDirector("Director");
        movie.setGenres(new ArrayList<>(Collections.singleton(MovieGenre.Horror)));
        movie.setActors(new ArrayList<>(Collections.singleton(new Actor())));
        return movie;
    }
}
