package niemelainen.movie.service.controller;

import niemelainen.movie.service.Logger;
import niemelainen.movie.service.dto.Movie;
import niemelainen.movie.service.enums.MovieGenre;
import niemelainen.movie.service.enums.StreamingService;
import niemelainen.movie.service.service.RecommendationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/recommendation")
public class RecommendationController extends Logger {

    @Autowired
    private RecommendationService recommendationService;

    @GetMapping
    public ResponseEntity<List<Movie>> findMovies(@RequestParam StreamingService streamingService, @RequestParam MovieGenre movieGenre, @RequestParam(required = false) Double minimumIMDBRating) {
        LOGGER.info("Searching movies with genre of {}.", movieGenre);
        List<Movie> movies = recommendationService.getMovies(streamingService, movieGenre, minimumIMDBRating);
        LOGGER.info("Found {} movies.", movies);
        return movies != null ? ResponseEntity.ok(movies) : ResponseEntity.badRequest().build();
    }
}
