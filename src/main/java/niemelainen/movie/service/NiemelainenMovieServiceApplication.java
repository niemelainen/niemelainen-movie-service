package niemelainen.movie.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NiemelainenMovieServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(NiemelainenMovieServiceApplication.class, args);
	}

}
