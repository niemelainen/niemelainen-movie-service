package niemelainen.movie.service.enums;

public enum StreamingService {
    Netflix,
    HBO,
    Prime
}
