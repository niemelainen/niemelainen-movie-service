package niemelainen.movie.service.enums;

public enum MovieGenre {
    Horror,
    Comedy,
    Romance,
    Thriller,
    Animation,
    Fantasy,
    Action,
    Mystery,
    Western
}
