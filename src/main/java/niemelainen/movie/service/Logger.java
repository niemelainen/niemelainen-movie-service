package niemelainen.movie.service;

import org.slf4j.LoggerFactory;

public abstract class Logger {

    protected final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(this.getClass());
}
