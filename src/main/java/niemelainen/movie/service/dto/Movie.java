package niemelainen.movie.service.dto;

import niemelainen.movie.service.enums.MovieGenre;

import java.util.List;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;

import static java.util.Collections.emptyList;

public class Movie {

    private String name;
    private String director;
    private List<Actor> actors = emptyList();
    @DecimalMin(value = "0.0", message = "Max rating for movie")
    @DecimalMax(value = "10.0", message = "Min rating for movie")
    private Double IMDBRating;
    private List<MovieGenre> genres = emptyList();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public List<Actor> getActors() {
        return actors;
    }

    public void setActors(List<Actor> actors) {
        this.actors = actors;
    }

    public Double getIMDBRating() {
        return IMDBRating;
    }

    public void setIMDBRating(Double IMDBrating) {
        this.IMDBRating = IMDBrating;
    }

    public List<MovieGenre> getGenres() {
        return genres;
    }

    public void setGenres(List<MovieGenre> genres) {
        this.genres = genres;
    }
}
